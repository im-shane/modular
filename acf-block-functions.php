<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	// accessible and dynamic accordion block
	$accordion_block = [
		'name' => 'accordion-block',
		'title' => __( 'Accordion Block', 'mod' ),
		'description' => __( 'Creates an accordion; The content is folded into the title.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'insert',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'block', 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $accordion_block );

	// accessible and dynamic gallery (images or videos)
	$gallery_block = [
		'name' => 'gallery-block',
		'title' => __( 'Gallery Block', 'mod' ),
		'description' => __( 'Creates a gallery block that displays up to 10 images/videos.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'format-gallery',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'block', 'gallery', 'categories', 'grid', 'image' ]
	];
	acf_register_block_type( $gallery_block );

	$three_by_two_row = [
		'name' => 'three-by-two-row',
		'title' => __( '3x2 Row', 'mod' ),
		'description' => __( 'Creates a row with 3 columns on top and 2 columns below it.', 'mod' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'mod-blocks',
		'align' => 'wide',
		'icon' => 'format-gallery',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'block', 'three', 'by', 'two', 'column', 'row' ]
	];
	acf_register_block_type( $three_by_two_row );
endif;